import { defineConfig } from "vite"
import { svelte } from "@sveltejs/vite-plugin-svelte"
import precompileIntl from "svelte-intl-precompile/sveltekit-plugin.js"
import dts from "vite-plugin-dts"
import { resolve } from "path"

export default defineConfig({
    plugins: [svelte(), precompileIntl("translations", "$translations"), dts()],
    server: {
        port: 8080,
        strictPort: true,
    },
    // prevent vite from obscuring rust errors
    clearScreen: false,
    build: {
        // Tauri supports es2021
        target: ["es2021", "chrome100", "safari13"],
        // don't minify for debug builds
        minify: !process.env.TAURI_DEBUG ? "esbuild" : false,
        // produce sourcemaps for debug builds
        sourcemap: !!process.env.TAURI_DEBUG,
        lib: {
            entry: resolve(__dirname, "src/gui.ts"),
            name: "nebulous-svelte-ui",
        },
    },
    rollupOptions: {
        external: ["svelte"]
    },
    test: {
        globals: true,
        environment: "jsdom",
        coverage: {
            reporter: ["text"],
        },
    },
})
