import { writable, type Writable } from "svelte/store"
import { CategoryUtils, type Category } from "../../backendInterface"
import type { Time } from "../../backendInterface"

export const focusedOn: Writable<unknown | null> = writable(null)

/// Get all categories of a section with their parents included
export function sectionCategories(allCategories: Category[], categoryIds: number[]): Category[][] {
    return categoriesIncludedInList(categoryIds, null, allCategories)
}

/// Find all the paths to the given categories that include the given category, excluding subcategories of included categories
function categoriesIncluded(
    sectionCategories: number[],
    category: Category,
): Category[][] {
    return CategoryUtils.includedIn(category, sectionCategories)
        ? [[category]]
        : categoriesIncludedInList(
              sectionCategories,
              category,
              category.subcategories,
          )
}

/// Get all paths to reach the given section categories from the given starting point, pushing the given category onto all of the paths if given
function categoriesIncludedInList(
    sectionCategories: number[],
    category: Category | null,
    categories: Category[],
): Category[][] {
    let ret: Category[][] = []

    for (const subcategory of categories) {
        let included = categoriesIncluded(
            sectionCategories,
            subcategory,
        ).filter(v => v.length !== 0)

        if (category !== null) {
            for (const chain of included) {
                chain.push(category)
            }
        }

        ret = ret.concat(included)
    }

    return ret
}

const month = [
    "dates.months.jan",
    "dates.months.feb",
    "dates.months.mar",
    "dates.months.apr",
    "dates.months.may",
    "dates.months.jun",
    "dates.months.jul",
    "dates.months.aug",
    "dates.months.sep",
    "dates.months.oct",
    "dates.months.nov",
    "dates.months.dec",
]

export function timeString(time: Time, t: (v: string, args?: { values: Record<string, string | number> }) => string): string {
    if (time === "None") {
        return ""
    }

    if ("Date" in time) {
        return t("dates.date", {
            values: {
                month: t(month[time.Date.month]),
                day: time.Date.day,
                year: time.Date.year,
            },
        })
    }

    let range = time.Range

    if (range[0].year < range[1].year) {
        return t("dates.range", {
            values: {
                year1: range[0].year,
                month1: t(month[range[0].month]),
                day1: range[0].day,
                year2: range[1].year,
                month2: t(month[range[1].month]),
                day2: range[1].day,
            },
        })
    }

    if (range[0].month < range[1].month) {
        return t("dates.rangeYearSame", {
            values: {
                year: range[0].year,
                month1: t(month[range[0].month]),
                day1: range[0].day,
                month2: t(month[range[1].month]),
                day2: range[1].day,
            },
        })
    }

    return t("dates.rangeYearMonthSame", {
        values: {
            year: range[0].year,
            month: t(month[range[0].month]),
            day1: range[0].day,
            day2: range[1].day,
        },
    })
}
