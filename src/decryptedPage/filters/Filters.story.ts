import { writable } from "svelte/store"
import Filters from "./Filters.svelte"

export default {
    title: "Filters",
    component: Filters,
}

export const Primary = {
    render: () => {
        return {
            Component: Filters,
            props: {
                decryptedInterfaceSubset: {
                    filters: {
                        selectedCategories: writable([]),
                        timeOption: writable(null),
                        textSearch: writable(""),
                        categoryFilterType: writable("Any")
                    },
                    categories: writable([]),
                    createCategory: console.log
                }
            },
        }
    },
}
