import { writable } from "svelte/store"
import Settings from "./Settings.svelte"

export default {
    title: "Settings",
    component: Settings,
}

interface Args {
    allowCheckForUpdates: boolean
    encryption: boolean | null
    allowChangeEncryption: boolean
}

export const Primary = {
    render: (args: Args) => {
        return {
            Component: Settings,
            props: {
                backendInterfaceSubset: {
                    settings: {
                        language: writable("en"),
                        categoryPrediction: writable(true),
                        checkForUpdates: writable(
                            args.allowCheckForUpdates ? true : null,
                        ),
                    },
                    encryption: writable(args.encryption)
                },
                changeEncryption: args.allowChangeEncryption ? async () => {} : null
            },
        }
    },
    args: {
        allowCheckForUpdates: true,
        encryption: true,
        allowChangeEncryption: true,
    }
}
