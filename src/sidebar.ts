import { writable } from "svelte/store"

export const sidebarsOpen = writable(1)
