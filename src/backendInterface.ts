import { get, type Readable, type Writable } from "svelte/store"

export type Level = "ERROR" | "WARN " | "INFO " | "DEBUG" | "TRACE"

export function levelNumber(level: Level): number {
  switch (level) {
    case "TRACE": return 1
    case "DEBUG": return 2
    case "INFO ": return 3
    case "WARN ": return 4
    case "ERROR": return 5
  }
}

export function levelName(num: number): Level {
  switch (num) {
    case 1: return "TRACE"
    case 2: return "DEBUG"
    case 3: return "INFO "
    case 4: return "WARN "
    case 5: return "ERROR"
    default: throw `The number ${num} doesn't represent a log level`
  }
}

export interface BackendInterface {
  readonly settings: Settings
  readonly encryption: Readable<boolean | null>
  readonly update: Readable<UpdateData | null>
  readonly error: Writable<ErrorData | null>
  readonly logs: Readable<Log[]>
  openDocs(title: string): void
  dataState(): Promise<"None" | "Encrypted" | DecryptedInterface | null>
  decryptDiary(password: string): Promise<DecryptedInterface | "IncorrectKey" | null>
  createDiary(password: string | null): Promise<DecryptedInterface | null>
  copyToClipboard(data: string): void
  log(level: Level, message: string): void
}

export interface DecryptedInterface {
  readonly filters: Filters
  readonly sections: Readable<Section[]>
  readonly categories: Readable<Category[]>
  readonly sectionsPassingFilter: Readable<unknown[]>
  createCategory(name: string): Promise<void>
  createSection(): Promise<void>
  changeEncryption(newPassword: string | null): Promise<void>
}

export interface ErrorData {
  readonly message: { FileDoesntExist: string } | { PermissionDenied: string } | null
  readonly details: string
}

export interface Log {
  level: number
  time: string
  module: string
  message: string
}

export function isErrorData(err: any): err is ErrorData {
  if (!(err instanceof Object)) return false

  if (!("message" in err && "details" in err)) return false

  if (typeof err.details !== "string") return false

  if (err.message !== null) {
    if (!(err.message instanceof Object)) return false

    if (
      !("FileDoesntExist" in err.message) ||
      typeof err.FileDoesntExist !== "string"
    )
      return false
    if (
      !("PermissionDenied" in err.message) ||
      typeof err.PermissionDenied !== "string"
    )
      return false
  }

  return true
}

export interface UpdateData {
  readonly version: string
  readonly notes: string | null
  installUpdate(): void
}

export interface DateValue {
  year: number
  month: number
  day: number
}

export type Time =
  | "None"
  | { Date: DateValue }
  | {
    Range: [DateValue, DateValue]
  }

export interface Section {
  readonly key: unknown
  readonly title: Writable<string>
  readonly time: Writable<Time>
  readonly suggestions: Readable<number[] | null>
  readonly categorySelector: CategorySelector

  getCurrentText(): Promise<string>
  updateText(text: string): Promise<void>
  delete(): Promise<void>
}

export interface CategorySelector {
  readonly categoryIds: Readable<number[]>
  
  addCategory(id: number): Promise<void>
  removeCategory(id: number): Promise<void>
}

export interface Category {
  readonly name: Writable<string>
  readonly subcategories: Category[]
  readonly color: Writable<string>
  readonly closed: Writable<boolean>
  readonly id: number

  delete(): Promise<void>
  changeParent(newParent: Category | null): Promise<void>
}

export class CategoryUtils {
  static includedIn(category: Category, ids: number[]): boolean {
    return ids.includes(category.id)
  }

  static toggleSelectedIn(category: Category, selector: CategorySelector) {
    if (get(selector.categoryIds).includes(category.id)) {
      selector.removeCategory(category.id)
    } else {
      selector.addCategory(category.id)
    }
  }

  static categoryFromId(id: number, categories: Category[]): Category | null {
    for (const category of categories) {
      if (category.id === id) {
        return category
      }

      const searchResultsFromThisCategory = CategoryUtils.categoryFromId(
        id,
        category.subcategories,
      )

      if (searchResultsFromThisCategory !== null) {
        return searchResultsFromThisCategory
      }
    }

    return null
  }
}

export interface Settings {
  readonly language: Writable<string>
  readonly categoryPrediction: Writable<boolean>
  readonly checkForUpdates: Writable<boolean | null>
}

export interface Filters {
  readonly timeOption: Writable<Time | null>
  readonly textSearch: Writable<string>
  readonly categoryFilterType: Writable<"Any" | "All" | "Not">
  readonly categorySelector: CategorySelector
}
