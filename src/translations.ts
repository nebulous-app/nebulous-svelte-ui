import {
    addMessages,
    getLocaleFromNavigator,
    init,
} from "svelte-intl-precompile"

// @ts-ignore, it exists
import en from "$translations/en"

addMessages("en", en)

init({
    fallbackLocale: "en",
    initialLocale: getLocaleFromNavigator() ?? undefined,
})
