import { writable } from "svelte/store"
import type { Writable } from "svelte/store"

export type Data = any

interface CurrentlyDragging {
    channel: string
    data: Data
    successfulDropCallback(): void
    recursiveDraggableId: number
}

export let currentlyDragging: Writable<CurrentlyDragging | null> =
    writable(null)
