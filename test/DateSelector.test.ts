import { cleanup, fireEvent, render, screen } from "@testing-library/svelte"
import DateSelector from "../src/decryptedPage/filters/dates/DateSelector.svelte"
import { describe, afterEach, it, expect, vi } from "vitest"
import {TestingStore} from "./util"

describe("DateSelector.svelte", () => {
    afterEach(() => cleanup())

    it("mounts and updated when input changes", async () => {
        let date = new TestingStore()

        date.initialize({ year: 2022, month: 4, day: 16 })
        
        const { container } = render(DateSelector, {
            date
        })

        expect(container).toBeTruthy()

        let elt = screen.getByTestId("date-selector") as HTMLInputElement
        expect(elt.value).toBe("2022-05-16")
        expect(date.set).not.toHaveBeenCalled()

        elt.value = "2024-01-29"

        await fireEvent.input(elt)

        expect(date.set).toHaveBeenCalledWith({ year: 2024, month: 0, day: 29 })
    })
})
