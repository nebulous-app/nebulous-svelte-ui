import { setTimeout } from "timers/promises"
import { cleanup, fireEvent, render, screen } from "@testing-library/svelte"
import CreationScreen from "../src/CreationScreen.svelte"
import { describe, afterEach, it, expect, vi } from "vitest"
import {
    TestingBackendInterface,
    TestingDecryptedInterface,
} from "./mockBackendInterface"
import "./util"

describe("CreationScreen.svelte", () => {
    afterEach(() => cleanup())

    it("creates an unencrypted diary", async () => {
        let backendInterface = new TestingBackendInterface()
        let decryptedInterface = new TestingDecryptedInterface()
        let updateState = vi.fn()

        backendInterface.createDiary.mockReturnValue(
            Promise.resolve(decryptedInterface),
        )

        const { container } = render(CreationScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        expect(container).toBeTruthy()

        expect(backendInterface.createDiary).not.toHaveBeenCalled()
        screen.getByTestId("no-encryption").click()
        expect(backendInterface.createDiary).toHaveBeenCalledWith(null)
        await setTimeout(0)
        expect(updateState).toHaveBeenCalledWith(decryptedInterface)
        expect(screen.getByTestId("err").innerHTML).toBe("")
    })

    it("creates an encrypted diary", async () => {
        let backendInterface = new TestingBackendInterface()
        let decryptedInterface = new TestingDecryptedInterface()
        let updateState = vi.fn()

        backendInterface.createDiary.mockReturnValue(
            Promise.resolve(decryptedInterface),
        )

        const { container } = render(CreationScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        let passwordInput = screen.getByTestId("password") as HTMLInputElement
        let confirmPasswordInput = screen.getByTestId(
            "confirm-password",
        ) as HTMLInputElement

        passwordInput.value = "a"
        await fireEvent.input(passwordInput)
        confirmPasswordInput.value = "a"
        await fireEvent.input(confirmPasswordInput)

        await fireEvent.click(screen.getByTestId("submit-password"))

        expect(screen.getByTestId("err").innerHTML).toBe("")
        expect(backendInterface.createDiary).toHaveBeenCalledWith("a")
        await setTimeout(0)
        expect(updateState).toHaveBeenCalledWith(decryptedInterface)
    })

    it("rejects empty passwords", async () => {
        let backendInterface = new TestingBackendInterface()
        let updateState = vi.fn()

        const { container } = render(CreationScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        await fireEvent.click(screen.getByTestId("submit-password"))

        expect(screen.getByTestId("err").innerHTML).not.toBe("")
        expect(backendInterface.createDiary).not.toHaveBeenCalled()
        expect(updateState).not.toHaveBeenCalled()
    })

    it("rejects different passwords", async () => {
        let backendInterface = new TestingBackendInterface()
        let updateState = vi.fn()

        const { container } = render(CreationScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        let passwordInput = screen.getByTestId("password") as HTMLInputElement
        let confirmPasswordInput = screen.getByTestId(
            "confirm-password",
        ) as HTMLInputElement

        passwordInput.value = "a"
        await fireEvent.input(passwordInput)
        confirmPasswordInput.value = "b"
        await fireEvent.input(confirmPasswordInput)

        await fireEvent.click(screen.getByTestId("submit-password"))

        expect(screen.getByTestId("err").innerHTML).not.toBe("")
        expect(backendInterface.createDiary).not.toHaveBeenCalled()
        expect(updateState).not.toHaveBeenCalled()
    })
})
