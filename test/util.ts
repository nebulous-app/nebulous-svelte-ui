// @ts-ignore, it exists
import en from "$translations/en.js"
import {
    addMessages,
    getLocaleFromNavigator,
    init,
} from "svelte-intl-precompile"
import type {
    Subscriber,
    Unsubscriber,
    Writable,
} from "svelte/store"
import { type Mock, vi } from "vitest"

addMessages("en", en)

init({
    fallbackLocale: "en",
    initialLocale: getLocaleFromNavigator() ?? undefined,
})

export class TestingStore<T> implements Writable<T> {
    constructor() {
        this.callSubscribers = vi.fn(v => {
            for (const subscriber of this.subscriptions) {
                subscriber(v)
            }
        })

        this.subscribe = vi.fn(subscriber => {
            if (this.value === "Uninitialized") {
                throw new Error("Testing store not initialized")
            }

            subscriber(this.value.value)

            this.subscriptions.add(subscriber)
            return () => {
                this.subscriptions.delete(subscriber)
            }
        })

        this.set = vi.fn(v => {
            if (this.value === "Uninitialized") {
                throw new Error("Testing store not initialized")
            }

            this.value = { value: v }
            this.callSubscribers(this.value.value)
        })

        this.update = vi.fn(fn => {
            if (this.value === "Uninitialized") {
                throw new Error("Testing store not initialized")
            }

            this.value = { value: fn(this.value.value) }
            this.callSubscribers(this.value.value)
        })
    }

    private value: "Uninitialized" | { value: T } = "Uninitialized"

    private subscriptions: Set<Subscriber<T>> = new Set()

    callSubscribers: Mock<[T], void>
    subscribe: Mock<[run: Subscriber<T>], Unsubscriber>
    set: Mock<[T], void>
    update: Mock<[(value: T) => T], void>

    get(): T {
        if (this.value === "Uninitialized") {
            throw new Error("Testing store not initialized")
        }

        return this.value.value
    }

    initialize(v: T) {
        this.value = { value: v }
    }
}

