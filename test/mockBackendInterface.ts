import { type Mock, vi } from "vitest"
import type {
    BackendInterface,
    Category,
    DecryptedInterface,
    ErrorData,
    Filters,
    Section,
    SectionEdit,
    Settings,
    Time,
    UpdateData,
} from "../src/backendInterface"
import { TestingStore } from "./util"

export class TestingBackendInterface implements BackendInterface {
    settings = new TestingSettings()
    encryption = new TestingStore<boolean | null>()
    update = new TestingStore<UpdateData | null>()
    error = new TestingStore<ErrorData | null>()
    openDocs = vi.fn<[string]>()
    dataState = vi.fn<
        [],
        Promise<"None" | "Encrypted" | TestingDecryptedInterface>
    >()
    decryptDiary = vi.fn<
        [string],
        Promise<TestingDecryptedInterface | "IncorrectKey" | null>
    >()
    createDiary = vi.fn<
        [string | null],
        Promise<TestingDecryptedInterface | null>
    >()
    copyToClipboard = vi.fn<[string], void>()
}

export class TestingDecryptedInterface implements DecryptedInterface {
    filters = new TestingFilters()
    sections = new TestingStore<TestingSection[]>()
    categories = new TestingStore<Category[]>()
    sectionsPassingFilter = new TestingStore<unknown[]>()
    createCategory = vi.fn<[string], Promise<void>>(Promise.resolve)
    createSection = vi.fn<[], Promise<void>>(Promise.resolve)
    changeEncryption: Mock<[string | null], Promise<void>> = vi.fn()
}

export class TestingSettings implements Settings {
    language = new TestingStore<string>()
    categoryPrediction = new TestingStore<boolean>()
    checkForUpdates = new TestingStore<boolean | null>()
}

export class TestingFilters implements Filters {
    selectedCategories = new TestingStore<number[]>()
    timeOption = new TestingStore<Time | null>()
    textSearch = new TestingStore<string>()
    categoryFilterType = new TestingStore<"Any" | "All" | "Not">()
}

export class TestingSection implements Section {
    constructor(key: string) {
        this.key = key
        this.title = new TestingStore()
        this.time = new TestingStore()
        this.categoryIds = new TestingStore()
        this.edits = new TestingStore()
        this.suggestions = new TestingStore()
    }

    key: string
    title: TestingStore<string>
    time: TestingStore<Time>
    categoryIds: TestingStore<number[]>
    edits: TestingStore<SectionEdit[]>
    suggestions: TestingStore<number[] | null>

    updateText = vi.fn<[string], Promise<void>>(async text => {
        this.edits.update(edits => {
            edits.push({
                timestamp: performance.now(),
                text,
                media: [],
            })
            return edits
        })
    })

    delete = vi.fn<[], Promise<void>>(Promise.resolve)
}
