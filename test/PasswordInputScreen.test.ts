import { cleanup, fireEvent, render, screen } from "@testing-library/svelte"
import PasswordInputScreen from "../src/PasswordInputScreen.svelte"
import { describe, afterEach, it, expect, vi } from "vitest"
import "./util"
import { TestingDecryptedInterface } from "./mockBackendInterface"

describe("PasswordInputScreen.svelte", () => {
    afterEach(() => cleanup())

    it("decrypts the diary", async () => {
        let backendInterface = { decryptDiary: vi.fn() }
        let updateState = vi.fn()

        let decryptedInterface = new TestingDecryptedInterface()
        
        backendInterface.decryptDiary.mockReturnValue(decryptedInterface)

        const { container } = render(PasswordInputScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        expect(container).toBeTruthy()

        expect(backendInterface.decryptDiary).not.toHaveBeenCalled()

        let passwordInput = screen.getByTestId(
            "password-input",
        ) as HTMLInputElement

        passwordInput.value = "a"
        fireEvent.input(passwordInput)

        await screen.getByTestId("submit").click()
        expect(backendInterface.decryptDiary).toHaveBeenCalledWith("a")
        expect(updateState).toHaveBeenCalledWith(decryptedInterface)
        expect(screen.getByTestId("err").innerHTML).toBe("")
    })

    it("rejects empty passwords", async () => {
        let backendInterface = { decryptDiary: vi.fn() }
        let updateState = vi.fn()
        
        backendInterface.decryptDiary.mockReturnValue(true)

        const { container } = render(PasswordInputScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        await screen.getByTestId("submit").click()
        expect(backendInterface.decryptDiary).not.toHaveBeenCalled()
        expect(updateState).not.toHaveBeenCalled()
        expect(screen.getByTestId("err").innerHTML).not.toBe("")
    })

    it("rejects wrong passwords", async () => {
        let backendInterface = { decryptDiary: vi.fn() }
        let updateState = vi.fn()
        
        backendInterface.decryptDiary.mockReturnValue("IncorrectKey")

        const { container } = render(PasswordInputScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        let passwordInput = screen.getByTestId(
            "password-input",
        ) as HTMLInputElement

        passwordInput.value = "a"
        fireEvent.input(passwordInput)

        await screen.getByTestId("submit").click()
        await setTimeout(() => {})
        expect(backendInterface.decryptDiary).toHaveBeenCalledWith("a")
        expect(updateState).not.toHaveBeenCalled()
        expect(screen.getByTestId("err").innerHTML).not.toBe("")
    })
    
    it("submits on enter pressed", async () => {
        let backendInterface = { decryptDiary: vi.fn() }
        let updateState = vi.fn()
        
        backendInterface.decryptDiary.mockReturnValue(true)

        const { container } = render(PasswordInputScreen, {
            backendInterfaceSubset: backendInterface,
            updateState,
        })

        let passwordInput = screen.getByTestId(
            "password-input",
        ) as HTMLInputElement

        passwordInput.value = "a"
        fireEvent.input(passwordInput)

        await fireEvent.keyDown(passwordInput, {key: "Enter"})
        expect(backendInterface.decryptDiary).toHaveBeenCalled()
    })
})
