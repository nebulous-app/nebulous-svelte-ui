import { setTimeout } from "timers/promises"
import { cleanup, fireEvent, render, screen } from "@testing-library/svelte"
import Dates from "../src/decryptedPage/filters/dates/Dates.svelte"
import { describe, afterEach, it, expect, vi } from "vitest"
import { TestingStore } from "./util"

describe("Dates.svelte", () => {
    afterEach(() => cleanup())

    it("mounts given None", async () => {
        let time = new TestingStore()

        time.initialize("None")

        const { container } = render(Dates, {
            time
        })

        expect(container).toBeTruthy()

        await setTimeout(0)

        expect(
            (screen.getByTestId("time-type-selector") as HTMLInputElement)
                .value,
        ).toBe("None")
        expect(screen.queryByTestId("date-selector")).toBe(null)

        expect(time.set).not.toHaveBeenCalled()
    })

    it("mounts given Date", async () => {
        let time = new TestingStore()

        time.initialize({
            Date: { year: 2022, month: 3, day: 21 }
        })

        const { container } = render(Dates, {
            time
        })

        expect(container).toBeTruthy()

        await setTimeout(0)

        expect(
            (screen.getByTestId("time-type-selector") as HTMLInputElement)
                .value,
        ).toBe("Date")
        expect(
            (screen.getByTestId("date-selector") as HTMLInputElement).value,
        ).toBe("2022-04-21")

        expect(time.set).not.toHaveBeenCalled()
    })

    it("mounts given Range", async () => {
        let time = new TestingStore()

        time.initialize({
            Range: [
                { year: 2022, month: 3, day: 21 },
                { year: 2023, month: 11, day: 1 },
            ],
        })

        const { container } = render(Dates, {
            time
        })

        expect(container).toBeTruthy()

        await setTimeout(0)

        expect(
            (screen.getByTestId("time-type-selector") as HTMLInputElement)
                .value,
        ).toBe("Range")

        let dateSelectors = screen.queryAllByTestId("date-selector")

        expect(dateSelectors.length).toBe(2)
        expect((dateSelectors[0] as HTMLInputElement).value).toBe("2022-04-21")
        expect((dateSelectors[1] as HTMLInputElement).value).toBe("2023-12-01")

        expect(time.set).not.toHaveBeenCalled()
    })

    it("saves date values", async () => {
        let time = new TestingStore()

        time.initialize({
            Range: [
                { year: 2022, month: 3, day: 21 },
                { year: 2023, month: 11, day: 1 },
            ],
        })

        const { container } = render(Dates, {
            time
        })

        expect(container).toBeTruthy()

        let timeTypeSelector = screen.getByTestId(
            "time-type-selector",
        ) as HTMLInputElement

        timeTypeSelector.value = "None"
        fireEvent.input(timeTypeSelector)
        expect(time.set).toHaveBeenCalledWith("None")

        timeTypeSelector.value = "Date"
        await fireEvent.input(timeTypeSelector)
        expect(time.set).toHaveBeenCalledWith({
            Date: { year: 2022, month: 3, day: 21 },
        })

        {
            ;(screen.getByTestId("date-selector") as HTMLInputElement).value =
                "2021-01-03"
        }
        await fireEvent.input(screen.getByTestId("date-selector"))
        expect(time.set).toHaveBeenCalledWith({
            Date: { year: 2021, month: 0, day: 3 },
        })

        timeTypeSelector.value = "Range"
        await fireEvent.input(timeTypeSelector)
        expect(time.set).toHaveBeenCalledWith({
            Range: [
                { year: 2021, month: 0, day: 3 },
                { year: 2023, month: 11, day: 1 },
            ],
        })

        let dateSelectors = screen.queryAllByTestId("date-selector")

        expect(dateSelectors.length).toBe(2)
        expect((dateSelectors[0] as HTMLInputElement).value).toBe("2021-01-03")
        expect((dateSelectors[1] as HTMLInputElement).value).toBe("2023-12-01")
    })
})
