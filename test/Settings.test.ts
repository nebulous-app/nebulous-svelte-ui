import Settings from "../src/Settings.svelte"
import { cleanup, fireEvent, render, screen } from "@testing-library/svelte"
import { afterEach, describe, expect, it, vi } from "vitest"
import { TestingBackendInterface } from "./mockBackendInterface"

function backendInterfaceSubset(language: string, categoryPrediction: boolean, checkForUpdates: boolean | null, encryption: boolean | null): TestingBackendInterface {
    let backendInterface = new TestingBackendInterface()
    
    backendInterface.settings.language.initialize(language)
    backendInterface.settings.categoryPrediction.initialize(categoryPrediction)
    backendInterface.settings.checkForUpdates.initialize(checkForUpdates)
    backendInterface.encryption.initialize(encryption)
    
    return backendInterface
}

describe("Settings.svelte", () => {
    afterEach(() => cleanup())

    it("allows changing category prediction", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, true)
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption: null,
        })
        
        let categoryPrediction = screen.queryByTestId("categoryPrediction") as HTMLInputElement
        
        expect(categoryPrediction.checked).toBe(true)
        expect(backendInterface.settings.categoryPrediction.callSubscribers).not.toHaveBeenCalled()
        
        categoryPrediction.click()
        
        expect(categoryPrediction.checked).toBe(false)
        expect(backendInterface.settings.categoryPrediction.callSubscribers).toHaveBeenCalledWith(false)

        categoryPrediction.click()

        expect(categoryPrediction.checked).toBe(true)
        expect(backendInterface.settings.categoryPrediction.callSubscribers).toHaveBeenCalledWith(true)
    })
    
    it("hides checking for updates when not available", async () => {
        let backendInterface = backendInterfaceSubset("en", true, null, true)
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption: null,
        })
        
        expect(screen.queryByTestId("checkForUpdates")).toBe(null)
    })
    
    it("allows changing checking for updates", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, true)
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption: null,
        })

        let checkForUpdates = screen.queryByTestId("checkForUpdates") as HTMLInputElement
        
        expect(checkForUpdates.checked).toBe(true)
        expect(backendInterface.settings.checkForUpdates.callSubscribers).not.toHaveBeenCalled()
        
        checkForUpdates.click()
        
        expect(checkForUpdates.checked).toBe(false)
        expect(backendInterface.settings.checkForUpdates.callSubscribers).toHaveBeenCalledWith(false)

        checkForUpdates.click()

        expect(checkForUpdates.checked).toBe(true)
        expect(backendInterface.settings.checkForUpdates.callSubscribers).toHaveBeenCalledWith(true)
    })
    
    it("allows enabling encryption", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, false)
        let changeEncryption = vi.fn()
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption,
        })
        
        let encryptionEnabled = screen.getByTestId("encryption-enabled") as HTMLInputElement
        
        expect(encryptionEnabled.checked).toBe(false)
        
        await encryptionEnabled.click()
        
        expect(encryptionEnabled.checked).toBe(true)
        
        let password = screen.getByTestId("password") as HTMLInputElement
        let confirmPassword = screen.getByTestId("confirm-password") as HTMLInputElement
        
        password.value = "a"
        fireEvent.input(password)
        confirmPassword.value = "a"
        fireEvent.input(confirmPassword)
        
        let confirmEncryptionChanges = screen.getByTestId("confirm-encryption-changes")
        
        await confirmEncryptionChanges.click()
        
        expect(changeEncryption).toHaveBeenCalledWith("a")
    })

    it("allows changing password", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, true)
        let changeEncryption = vi.fn()
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption
        })
        
        let encryptionEnabled = screen.getByTestId("encryption-enabled") as HTMLInputElement
        
        expect(encryptionEnabled.checked).toBe(true)
        
        let password = screen.getByTestId("password") as HTMLInputElement
        let confirmPassword = screen.getByTestId("confirm-password") as HTMLInputElement
        
        password.value = "a"
        fireEvent.input(password)
        confirmPassword.value = "a"
        await fireEvent.input(confirmPassword)
        
        let confirmEncryptionChanges = screen.getByTestId("confirm-encryption-changes")
        
        await confirmEncryptionChanges.click()
        
        expect(changeEncryption).toHaveBeenCalledWith("a")
    })

    it("rejects bad password inputs", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, false)
        let changeEncryption = vi.fn()
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption
        })
        
        let encryptionEnabled = screen.getByTestId("encryption-enabled") as HTMLInputElement
        
        await encryptionEnabled.click()
        
        expect(encryptionEnabled.checked).toBe(true)
        
        let password = screen.getByTestId("password") as HTMLInputElement
        let confirmPassword = screen.getByTestId("confirm-password") as HTMLInputElement
        
        expect(password.value).toBe("")
        expect(confirmPassword.value).toBe("")
        
        let confirmEncryptionChanges = screen.getByTestId("confirm-encryption-changes")
        
        await confirmEncryptionChanges.click()
        
        let err = screen.getByTestId("err")
        
        expect(changeEncryption).not.toHaveBeenCalled()
        
        expect(err.innerHTML).not.toBe("")

        let prevErr = err.innerHTML
        
        password.value = "a"
        fireEvent.input(password)
        
        await confirmEncryptionChanges.click()
        
        expect(err.innerHTML).not.toBe("")
        expect(err.innerHTML).not.toBe(prevErr)
    })

    it("allows disabling encryption", async () => {
        let backendInterface = backendInterfaceSubset("en", true, true, true)
        let changeEncryption = vi.fn()
        
        let { container } = render(Settings, {
            backendInterfaceSubset: backendInterface,
            changeEncryption
        })
        
        let encryptionEnabled = screen.getByTestId("encryption-enabled") as HTMLInputElement
        
        expect(encryptionEnabled.checked).toBe(true)
        
        await encryptionEnabled.click()

        expect(encryptionEnabled.checked).toBe(false)
        
        let confirmEncryptionChanges = screen.getByTestId("confirm-encryption-changes")
        
        await confirmEncryptionChanges.click()
        
        expect(changeEncryption).toHaveBeenCalledWith(null)
    })
})
