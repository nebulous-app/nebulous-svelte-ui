import { writable, type Writable } from "svelte/store"
import {
    type CategorySelector,
    levelNumber,
    type BackendInterface,
    type Category,
    type DecryptedInterface,
    type ErrorData,
    type Filters,
    type Level,
    type Log,
    type Section,
    type Settings,
    type Time,
    type UpdateData,
} from "../src/backendInterface"

export class StorybookBackendInterface implements BackendInterface {
    settings = new StorybookSettings()
    encryption = writable<boolean | null>(false)
    update = writable<UpdateData | null>(null)
    error = writable<ErrorData | null>(null)
    logs = writable<Log[]>([])

    openDocs() {
        throw new Error(
            "Opening documentation not implemented in storybook environment",
        )
    }

    async dataState(): Promise<
        "None" | "Encrypted" | StorybookDecryptedInterface | null
    > {
        return new StorybookDecryptedInterface()
    }

    async decryptDiary(
        passwd: string,
    ): Promise<StorybookDecryptedInterface | "IncorrectKey" | null> {
        return new StorybookDecryptedInterface()
    }

    async createDiary(
        passwd: string | null,
    ): Promise<StorybookDecryptedInterface | null> {
        return new StorybookDecryptedInterface()
    }

    copyToClipboard(text: string) {
        throw new Error(
            "Copying to clipboard not implemented in storybook environment",
        )
    }

    log(level: Level, message: string) {
        this.logs.update(logs => {
            logs.push({
                time: Date.now().toLocaleString(),
                level: levelNumber(level),
                module: "frontend",
                message
            })

            return logs
        })
    }
}

export class StorybookDecryptedInterface implements DecryptedInterface {
    filters = new StorybookFilters()
    sections = writable<StorybookSection[]>([])
    categories = writable<StorybookCategory[]>([])
    sectionsPassingFilter = writable<unknown[]>()

    async createCategory(name: string) {
        throw new Error(
            "Creating category not implemented in storybook environment",
        )
    }

    async createSection() {
        throw new Error(
            "Creating section not implemented in storybook environment",
        )
    }

    async changeEncryption(passwd: string | null) {
        throw new Error(
            "Creating category not implemented in storybook environment",
        )
    }
}

export class StorybookSettings implements Settings {
    language = writable<string>("en")
    categoryPrediction = writable<boolean>(false)
    checkForUpdates = writable<boolean | null>(false)
}

export class StorybookFilters implements Filters {
    categorySelector: CategorySelector = new StoryCategorySelector([])
    timeOption = writable<Time | null>(null)
    textSearch = writable<string>("")
    categoryFilterType = writable<"Any" | "All" | "Not">("All")
}

export class StorybookCategory implements Category {
    constructor(name: string, color: string, id: number) {
        this.name = writable(name)
        this.subcategories = []
        this.color = writable(color)
        this.closed = writable(false)
        this.id = id
    }

    name: Writable<string>
    subcategories: Category[]
    color: Writable<string>
    closed: Writable<boolean>
    id: number

    delete(): Promise<void> {
        throw new Error("Method not implemented.")
    }

    changeParent(newParent: Category | null): Promise<void> {
        throw new Error("Method not implemented.")
    }
}

export class StorybookSection implements Section {
    constructor(
        key: string,
        title: string,
        time: Time,
        categoryIds: number[],
        text: string,
    ) {
        this.key = key
        this.title = writable(title)
        this.time = writable(time)
        this.text = text
        this.categorySelector = new StoryCategorySelector(categoryIds)
    }

    key: string
    title: Writable<string>
    time: Writable<Time>
    suggestions = writable(null)
    categorySelector: CategorySelector
    private text: string

    async getCurrentText(): Promise<string> {
        return this.text
    }

    async updateText(text: string) {
        this.text = text
    }

    async delete() {
        throw new Error(
            "Deleting sections not implemented in storybook environment",
        )
    }
}

// Not correct but good enough for storybook
class StoryCategorySelector implements CategorySelector {
    constructor(categoryIds: number[]) {
        this.categoryIds = writable(categoryIds)
    }

    categoryIds: Writable<number[]> = writable([])
    
    async addCategory(id: number): Promise<void> {
        this.categoryIds.update((v) => {
            if (v.includes(id)) {
                return v
            }

            v.push(id)
            return v
        })
    }

    async removeCategory(id: number): Promise<void> {
        this.categoryIds.update((v) => {
            v.filter(v => v != id)
            return v
        })
    }
}
